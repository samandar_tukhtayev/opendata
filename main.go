package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func main() {
	// OPEN DATA ga HTTP so'rovni yaratish
	endpoint := "https://api-dtp.yhxbb.uz/api/egov/open_data/info_car?format=json&plate_number="
	plate_number := "95E603HA"
	tech_pass := "AAG0301102"
	requestParams := url.Values{}
	requestParams.Add("plate_number", plate_number)
	requestParams.Add("tech_pass", tech_pass)

	requestURL := endpoint + "&" + requestParams.Encode()
	req, err := http.NewRequest("GET", requestURL, nil)
	if err != nil {
		log.Println(err)
	}

	// HTTP so'rovni jo'natish
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error while response info to yandex api: ", err.Error())
	}
	defer resp.Body.Close()

	// JSON tahlil qilish
	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		log.Println("Error while decoding response info: ", err.Error())
	}
	fmt.Println(data)
}
